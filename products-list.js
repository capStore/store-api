const Products = require("./products");
const products = require("./productsMock");

class ProductsList {
  constructor() {
    this.list = products.map(product => new Products(product))
  }

  getList(){
    return this.list;
  }

  addProduct (data) {
    const newProduct = new Products(data)
    this.list.push(newProduct)
    return this.list;
  }
  getByID (id) {
    return this.list.find(product => product.id === id);
  }
  removeProduct (id) {
    this.list = this.list.filter(product => product.id !== id)
  }
}

module.exports = ProductsList;