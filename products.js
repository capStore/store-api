const { v4: uuidv4 } = require('uuid')
class Products {
  constructor({ price, brand, model, ram, cpu, display, camera, battery, so, dimensions, weight, image }) {
    this.id = uuidv4();
    this.price = price,
    this.brand = brand,
    this.model = model,
    this.ram = ram,
    this.cpu = cpu,
    this.display = display,
    this.camera = camera,
    this.battery = battery,
    this.so = so,
    this.dimensions = dimensions,
    this.weight = weight,
    this.image = image
  }
}

module.exports = Products;