const express = require('express');
const cors = require('cors');
const ProductsList = require('./products-list');
const CartList = require('./cart-list');

class Server {

  constructor() {
    this.app = express();
    this.port = process.env.PORT;
    this.productList = new ProductsList();
    this.cartList = new CartList();
  }
  middlewares() {
    this.app.use(cors())
    this.app.use(express.json())
  }
  execute() {
    this.middlewares();

    this.app.get('/product', (_, res) => res.json({ error: false, data: this.productList.getList() }))
    this.app.get('/product/:id', (req, res) => res.json({ error: false, data: this.productList.getByID(req.params.id) }))

    this.app.get('/cart', (_, res) => res.json({ error: false, data: this.cartList.getList() }))
    this.app.post('/cart', ({ body }, res) =>  res.json({ error: false, data: this.cartList.add({...body, product: this.productList.getByID(body.id)}) }))
    this.app.delete('/cart/:id', (req, res) => res.json({ error: false, data: this.cartList.remove(req.params.id) }))


    this.app.listen(this.port, () => {
      console.log('Server corriendo en puerto:', this.port || 4000);
    });
  }

}

module.exports = Server;