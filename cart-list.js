class CartList {
  constructor (){
    this.cart = [];
  }
  getList(){
    return this.cart;
  }
  add (data) {
    this.cart.push(data)
    return this.cart;
  }
  getByID (id) {
    return this.cart.find(cart => cart.id === id);
  }
  remove (id) {
    this.cart = this.cart.filter(cart => cart.id !== id)
    return this.cart
  }
}

module.exports = CartList;